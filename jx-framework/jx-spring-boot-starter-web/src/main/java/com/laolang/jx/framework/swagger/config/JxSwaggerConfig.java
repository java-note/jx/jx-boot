package com.laolang.jx.framework.swagger.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
@Configuration
public class JxSwaggerConfig {

    @Bean
    public GroupedOpenApi adminApi() {
        String[] paths = {"/**"};
        String[] packagedToMatch = {"com.laolang.jx.module.system.controller.admin.dict"};
        return GroupedOpenApi.builder().group("admin")
            .pathsToMatch(paths)
            .packagesToScan(packagedToMatch).build();
    }

    @Bean
    public OpenAPI openAPI() {
        return new OpenAPI()
            .info(new Info()
                .title("京西")
                .version("0.1")
                .description("京西 API")
                .termsOfService("https://gitlab.com/java-note/jx")
                .license(new License().name("MIT").url("https://gitlab.com/java-note/jx"))
            );
    }
}
