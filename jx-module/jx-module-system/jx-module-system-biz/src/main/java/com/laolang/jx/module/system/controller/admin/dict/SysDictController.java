package com.laolang.jx.module.system.controller.admin.dict;

import com.laolang.jx.framework.common.domain.R;
import com.laolang.jx.module.system.controller.admin.dict.rsp.dict.SysDictListRsp;
import com.laolang.jx.module.system.logic.admin.dict.SysDictLogic;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
@Tag(name="admin - 字典管理")
@RequiredArgsConstructor
@RequestMapping("system/dict")
@RestController
public class SysDictController {

    private final SysDictLogic sysDictLogic;

    @Operation(summary = "字典数据列表")
    @PostMapping("list")
    public R<SysDictListRsp> list(){
        return R.ok(sysDictLogic.list());
    }

}
