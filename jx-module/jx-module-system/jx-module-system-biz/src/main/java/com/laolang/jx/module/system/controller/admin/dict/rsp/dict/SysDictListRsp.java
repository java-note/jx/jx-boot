package com.laolang.jx.module.system.controller.admin.dict.rsp.dict;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
@Schema(description = "admin - 字典数据分页列表 req vo")
@Data
public class SysDictListRsp {

    @Schema(description = "id")
    private Long id;
    @Schema(description = "名称")
    private String name;
}
