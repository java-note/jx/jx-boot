package com.laolang.jx.module.system.logic.admin.dict;

import com.laolang.jx.module.system.controller.admin.dict.rsp.dict.SysDictListRsp;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
@Slf4j
@Service
public class SysDictLogic {

    public SysDictListRsp list() {
        log.info("admin system dict list");
        SysDictListRsp rsp = new SysDictListRsp();
        rsp.setId(1001L);
        rsp.setName("性别");
        return rsp;
    }
}
