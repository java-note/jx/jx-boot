package com.laolang.jx;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
@Slf4j
@SpringBootApplication
public class JxApplication {

    public static void main(String[] args) {
        SpringApplication.run(JxApplication.class,args);
        log.info("jx is running...");
    }
}
